import Head from 'next/head'
import styles from '../styles/Home.module.css'
import React, { useState } from 'react';

export default function Home() {
  const [pages, setPages] = useState(200);
  const handleOnChange = event => {
    const { value } = event.target;
    console.log(pages)
    setPages(value);
  };
  return (
    <div className={styles.container}>
      <Head>
        <title>Page2Banana</title>
        <link rel="icon" href="/favicon.ico" />
      </Head>

      <main className={styles.main}>
        <h1 className={styles.title}>
          Welcome to Page2Banana
        </h1>

        <p className={styles.description}>
          Convert pages to banana{' '}
        </p>
        <div className={styles.grid}>
        <input type="number" onChange={handleOnChange} value={pages}></input>
        </div>
        {(pages > 0 && pages <250 ) ? <h3>Less than one</h3> : <h3>More than one</h3> }
      </main>

      <footer className={styles.footer}>
        <a
          href="https://vercel.com?utm_source=create-next-app&utm_medium=default-template&utm_campaign=create-next-app"
          target="_blank"
          rel="noopener noreferrer"
        >
          Powered by{' '}
          @lucaslo
        </a>
      </footer>
    </div>
  )
}
